<?php

namespace App\Controller\Styles;


use App\Repository\StyleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;

#[AsController]
class StylesRankingController extends AbstractController
{
	public function __construct(private StyleRepository $styleRepository)
	{}

	#[Route(path: 'api/styles/beer/ranking', name: '_beer_styles_beer_ranking', methods: ['GET'])]
	public function __invoke(): JsonResponse
	{
		$stylesRanking = $this->styleRepository->findPopularBeerStyle();
		return $this->json($stylesRanking);
	}
}
